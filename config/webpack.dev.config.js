const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common.config.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');

module.exports = webpackMerge(commonConfig, {
    devtool: 'source-map',
    output: {
        path: path.resolve('dist'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },
    devServer: {
        inline: true,
        contentBase: './',
        historyApiFallback: true
    },
})