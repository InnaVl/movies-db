### MOVIES DB

## To start application: ##

``npm start``

Project is running at http://localhost:8080/



## To build prod: ##

``npm run build:prod``


## To run unit test: ##

``npm test``
