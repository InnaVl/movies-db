module.exports = (env, options) => {
  if (options.mode === 'production' || options.mode === 'prod') {
    return require('./config/webpack.prod.config');
  }
  return require('./config/webpack.dev.config');
}
