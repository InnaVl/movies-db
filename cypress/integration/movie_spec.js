describe('MoviesDB', () => {
  it('<title> should be correct', () => {

    cy.visit('http://localhost:8080/')
    cy.title().should('include', 'Movies DB')

  })

  it('should open movie on click', () => {

    cy.visit('http://localhost:8080/');
    cy.get('.movie-preview').first().click();
    cy.get('.movie').should('be.visible');

  })
})
