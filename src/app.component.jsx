import React from 'react';

import Content from './components/movies-list/index';
import Footer from './components/footer/footer.component';
import SubHeader from './components/sub-header/sub-header.component';
import ErrorBoundary from './components/error-boundary/error-boundary.component';

export class App extends React.Component {

    render() {
        return (
            <ErrorBoundary>
                {this.props.children}
                <SubHeader />
                <Content />
                <Footer />
            </ErrorBoundary>
        );
    }
}