import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './configureStore';
import { Route, Switch, BrowserRouter as Router, } from 'react-router-dom';
import { NotFound } from './components/not-found/not-found.component';

import Home from './scenes/home/index';
import Movie from './scenes/movie/movie/movie.component'


import './assets/styles/style.css';
import './assets/styles/common.scss';

import { App } from './app.component';


ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <div className="main">
                <Router>
                    <App>
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/search" component={Home} />
                            <Route path="/movie/:id" render={(history) => <Movie {...history} />} />
                            <Route path="*" render={(location) => <NotFound {...location} />} />
                        </Switch>
                    </App>
                </Router>
            </div>
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);
