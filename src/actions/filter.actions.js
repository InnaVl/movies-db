import { type } from "os";

export const RESET_FILTER = 'RESET_FILTER';
export const SORT_BY_RATING = 'SORT_BY_RATING';
export const SORT_BY_RELEASE_DATE = 'SORT_BY_RELEASE_DATE';
export const CHANGE_SORT_ORDER_DECS = 'CHANGE_SORT_ORDER_DECS';
export const CHANGE_SORT_ORDER_ASC = 'CHANGE_SORT_ORDER_ASC';
export const SEARCH = 'SEARCH';
export const SEARCH_BY_CATEGORY = 'SEARCH_BY_CATEGORY';
export const FILTER_BY_GENRE = 'FILTER_BY_GENRE';

export function filterByGenre(genre) {
    return {
        type: FILTER_BY_GENRE,
        payload: genre
    };
}

export function searchByCategory(category) {
    return {
        type: SEARCH_BY_CATEGORY,
        payload: category
    };
}

export function search(searchStr) {
    return {
        type: SEARCH,
        payload: searchStr
    };
}
export function sortOrderAsc() {
    return {
        type: CHANGE_SORT_ORDER_ASC
    };
}

export function sortOrderDecs() {
    return {
        type: CHANGE_SORT_ORDER_DECS
    };
}

export function sortByReleaseDate() {
    return {
        type: SORT_BY_RELEASE_DATE
    };
}
export function sortByRating() {
    return {
        type: SORT_BY_RATING
    };

}
export function reset() {
    return { type: RESET_FILTER };
}