import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import * as moviesAction from '../actions/movies.actions';
import initialStore from '../reducers/reducer';
import fetchMock from 'fetch-mock';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const mockMovies = [
    {
        budget: 165000000,
        genres: ['Adventure', 'Drama', 'Science Fiction'],
        id: 157336,
        overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
        poster_path: 'https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
        release_date: '2014-11-05',
        revenue: 675120017,
        runtime: 169,
        tagline: 'Mankind was born on Earth. It was never meant to die here.',
        title: 'Interstellar',
        vote_average: 8.1,
        vote_count: 13627
    }
];

const mockError = 'An error occurred.';

describe('Movies actions', () => {

    it('check actions constants', () => {
        expect(moviesAction.REQUEST_ALL_MOVIES).toBe('REQUEST_ALL_MOVIES');
        expect(moviesAction.SUCCESS_RECEIVE_ALL_MOVIES).toBe('SUCCESS_RECEIVE_ALL_MOVIES');
        expect(moviesAction.ERROR_RECEIVE_ALL_MOVIES).toBe('ERROR_RECEIVE_ALL_MOVIES');
        expect(moviesAction.REQUEST_ALL_MOVIES_BY_GENRE).toBe('REQUEST_ALL_MOVIES_BY_GENRE');
    });

    it('should create an action to request movies', () => {
        const expectedAction = {
            type: moviesAction.REQUEST_ALL_MOVIES
        };
        expect(moviesAction.requestAllMovies(mockMovies)).toEqual(expectedAction);
    });

    it('should create an action on successful get movies request', () => {
        const expectedAction = {
            type: moviesAction.SUCCESS_RECEIVE_ALL_MOVIES,
            payload: mockMovies
        };
        expect(moviesAction.successReceiveAllMovies(mockMovies)).toEqual(expectedAction);
    });

    it('should create an action on error get movies request', () => {
        const expectedAction = {
            type: moviesAction.ERROR_RECEIVE_ALL_MOVIES,
            payload: mockError
        };
        expect(moviesAction.errorReceiveAllMovies(mockError)).toEqual(expectedAction);
    });

    describe('should handle fetch', () => {

        beforeEach(() => {
            fetchMock.reset();
            fetchMock.restore();
        });
        afterEach(() => {
            fetchMock.reset();
            fetchMock.restore();
        })

        it('fetch dispatch REQUEST_ALL_MOVIES action and SUCCESS_RECEIVE_ALL_MOVIES', async () => {
            fetchMock.get('*', JSON.stringify({ data: [mockMovies], total: 1 }));
            const store = mockStore({});
            const expectedActions = [
                { type: moviesAction.REQUEST_ALL_MOVIES },
                { type: moviesAction.SUCCESS_RECEIVE_ALL_MOVIES, payload: { movies: [mockMovies], total: 1 } }
            ];
            const result = await store.dispatch(moviesAction.fetchMovies());
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('Should add search params and return movie', async () => {
            fetchMock.get('*', JSON.stringify({ data: [mockMovies], total: 1 }));
            const store = mockStore({});
            const expectedActions = [
                { type: moviesAction.REQUEST_ALL_MOVIES },
                { type: moviesAction.SUCCESS_RECEIVE_ALL_MOVIES, payload: { movies: [mockMovies], total: 1 } }
            ];
            const result = await store.dispatch(moviesAction.fetchMovies({ search: 'Obli', searchBy: 'title' }));
            expect(fetchMock.calls()[0][0].searchParams.get('search')).toBe('Obli');
            expect(fetchMock.calls()[0][0].searchParams.get('searchBy')).toBe('title');
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('Should add search params and return movie', async () => {
            fetchMock.get('*', JSON.stringify({ data: [mockMovies], total: 1 }));
            const store = mockStore({});
            const expectedActions = [
                { type: moviesAction.REQUEST_ALL_MOVIES },
                { type: moviesAction.SUCCESS_RECEIVE_ALL_MOVIES, payload: { movies: [mockMovies], total: 1 } }
            ];
            const result = await store.dispatch(moviesAction.fetchMovies({ search: 'Obli' }));
            expect(fetchMock.calls()[0][0].searchParams.get('search')).toBe('Obli');
            expect(fetchMock.calls()[0][0].searchParams.get('searchBy')).toBe('title');
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('Should add search params and return movie', async () => {
            fetchMock.get('*', JSON.stringify({ data: [mockMovies], total: 1 }));
            const store = mockStore({});
            const expectedActions = [
                { type: moviesAction.REQUEST_ALL_MOVIES },
                { type: moviesAction.SUCCESS_RECEIVE_ALL_MOVIES, payload: { movies: [mockMovies], total: 1 } }
            ];
            const result = await store.dispatch(moviesAction.fetchMovies({ sortBy: 'rating' }));
            expect(fetchMock.calls()[0][0].searchParams.get('sortBy')).toBe('rating');
            expect(store.getActions()).toEqual(expectedActions);
        });

        it('Should add search params and return movie', async () => {
            fetchMock.get('*', JSON.stringify({ data: [mockMovies], total: 1 }));
            const store = mockStore({});
            const expectedActions = [
                { type: moviesAction.REQUEST_ALL_MOVIES },
                { type: moviesAction.SUCCESS_RECEIVE_ALL_MOVIES, payload: { movies: [mockMovies], total: 1 } }
            ];
            const result = await store.dispatch(moviesAction.fetchMovies({ sortBy: '' }));
            expect(fetchMock.calls()[0][0].searchParams.get('sortBy')).toBeNull();

            expect(store.getActions()).toEqual(expectedActions);
        });

        it('fetch dispatch REQUEST_ALL_MOVIES action and ERROR_RECEIVE_ALL_MOVIES', () => {
            fetchMock.get('*', Promise.reject('An error occurred.'));
            const store = mockStore({});
            store.clearActions();

            const expectedActions = [
                { type: moviesAction.REQUEST_ALL_MOVIES },
                { type: moviesAction.ERROR_RECEIVE_ALL_MOVIES, payload: 'An error occurred.' }
            ];
            return store.dispatch(moviesAction.fetchMovies())
                .then(() => {
                    expect(store.getActions()).toEqual(expectedActions);
                });

        });
    });

});
