import * as filterActions from './filter.actions';
describe('Filter and Sorting Actions', () => {

    it('actions constants should be define', () => {
        expect(filterActions.SORT_BY_RATING).toBe('SORT_BY_RATING');
        expect(filterActions.SORT_BY_RELEASE_DATE).toBe('SORT_BY_RELEASE_DATE');
        expect(filterActions.CHANGE_SORT_ORDER_DECS).toBe('CHANGE_SORT_ORDER_DECS');
        expect(filterActions.CHANGE_SORT_ORDER_ASC).toBe('CHANGE_SORT_ORDER_ASC');
        expect(filterActions.SEARCH).toBe('SEARCH');
        expect(filterActions.SEARCH_BY_CATEGORY).toBe('SEARCH_BY_CATEGORY');
        expect(filterActions.FILTER_BY_GENRE).toBe('FILTER_BY_GENRE');
    });

    it('should create action to filter by genre', () => {
        const genre = 'drama';
        const expectedAction = {
            type: filterActions.FILTER_BY_GENRE,
            payload: genre
        };
        expect(filterActions.filterByGenre(genre)).toEqual(expectedAction)
    });

    it('should create action to search by category', () => {
        const category = 'title';
        const expectedAction = {
            type: filterActions.SEARCH_BY_CATEGORY,
            payload: category
        };
        expect(filterActions.searchByCategory(category)).toEqual(expectedAction)
    });

    it('should create action to search by search string', () => {
        const searchStr = 'Harry Potter';
        const expectedAction = {
            type: filterActions.SEARCH,
            payload: searchStr
        };
        expect(filterActions.search(searchStr)).toEqual(expectedAction);
    });

    it('should create action to sort by Order -- Asc', () => {
        const expectedAction = {
            type: filterActions.CHANGE_SORT_ORDER_ASC
        };
        expect(filterActions.sortOrderAsc()).toEqual(expectedAction)
    });

    it('should create action to sort by Order -- Decs', () => {
        const expectedAction = {
            type: filterActions.CHANGE_SORT_ORDER_DECS
        };
        expect(filterActions.sortOrderDecs()).toEqual(expectedAction)
    });

    it('should create action to sort by release date', () => {
        const expectedAction = {
            type: filterActions.SORT_BY_RELEASE_DATE
        };
        expect(filterActions.sortByReleaseDate()).toEqual(expectedAction)
    });

    it('should create action to sort by rating', () => {
        const expectedAction = {
            type: filterActions.SORT_BY_RATING
        };
        expect(filterActions.sortByRating()).toEqual(expectedAction)
    });
});