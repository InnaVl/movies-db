export const REQUEST_MOVIE = 'REQUEST_MOVIE';
export const SUCCESS_RECEIVE_MOVIE = 'SUCCESS_RECEIVE_MOVIE';
export const ERROR_RECEIVE_MOVIE = 'ERROR_RECEIVE_MOVIE';
export const CLOSE_MOVIE = 'CLOSE_MOVIE';


export function fetchMovie(id) {
    return function (dispatch) {
        dispatch(requestMovie());
        return fetch(`http://react-cdp-api.herokuapp.com/movies/${id}`)
            .then(
            response => {
                return response.json();
            })
            .then((json => {
                dispatch(successReceiveMovie(json))
            }))
            .catch((error) => { dispatch(errorReceiveMovie('Error')) })
    }
}

export function requestMovie() {
    return {
        type: REQUEST_MOVIE
    };
}

export function successReceiveMovie(movie) {
    return {
        type: SUCCESS_RECEIVE_MOVIE,
        payload: movie
    };
}

export function errorReceiveMovie(error) {
    return {
        type: ERROR_RECEIVE_MOVIE,
        payload: error
    };
}


export function closeMovie() {
    return {
        type: CLOSE_MOVIE
    };
}
