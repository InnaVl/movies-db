import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import * as currentMovieActions from './current-movie.actions';
import { currentMovie } from '../reducers/current-movie.reducer';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const mockMovie = {
    budget: 165000000,
    genres: ['Adventure', 'Drama', 'Science Fiction'],
    id: 157336,
    overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
    poster_path: 'https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
    release_date: '2014-11-05',
    revenue: 675120017,
    runtime: 169,
    tagline: 'Mankind was born on Earth. It was never meant to die here.',
    title: 'Interstellar',
    vote_average: 8.1,
    vote_count: 13627
};

const mockError = 'Ooops';

describe('Current Movie Actions', () => {

    beforeEach(() => {
        fetchMock.reset();
        fetchMock.restore();
    });
    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore();
    })

    it('check actions constants', () => {
        expect(currentMovieActions.REQUEST_MOVIE).toBe('REQUEST_MOVIE');
        expect(currentMovieActions.SUCCESS_RECEIVE_MOVIE).toBe('SUCCESS_RECEIVE_MOVIE');
        expect(currentMovieActions.ERROR_RECEIVE_MOVIE).toBe('ERROR_RECEIVE_MOVIE');
        expect(currentMovieActions.CLOSE_MOVIE).toBe('CLOSE_MOVIE');
    });

    it('should create an action to request a movie', () => {
        const expectedAction = {
            type: currentMovieActions.REQUEST_MOVIE
        }
        expect(currentMovieActions.requestMovie()).toEqual(expectedAction);
    });

    it('should create an action to request a movie', () => {
        const expectedAction = {
            type: currentMovieActions.REQUEST_MOVIE
        }
        expect(currentMovieActions.requestMovie()).toEqual(expectedAction);
    });

    it('should create an action to handle successful movie request', () => {
        const expectedAction = {
            type: currentMovieActions.SUCCESS_RECEIVE_MOVIE,
            payload: mockMovie
        }
        expect(currentMovieActions.successReceiveMovie(mockMovie)).toEqual(expectedAction);
    });

    it('should create an action to handle error movie request', () => {
        const expectedAction = {
            type: currentMovieActions.ERROR_RECEIVE_MOVIE,
            payload: mockError
        }
        expect(currentMovieActions.errorReceiveMovie(mockError)).toEqual(expectedAction);
    });

    it('should create an action to close movie', () => {
        const expectedAction = {
            type: currentMovieActions.CLOSE_MOVIE,
        }
        expect(currentMovieActions.closeMovie()).toEqual(expectedAction);
    });

    it('Should create REQUEST_MOVIE action and SUCCESS_RECEIVE_MOVIE action on successful fetch', async () => {

        fetchMock.get('*', JSON.stringify(mockMovie));

        const expectedActions = [
            { type: currentMovieActions.REQUEST_MOVIE },
            { type: currentMovieActions.SUCCESS_RECEIVE_MOVIE, payload: mockMovie }
        ];

        const store = mockStore({ currentMovie: null });
        const result = await store.dispatch(currentMovieActions.fetchMovie(mockMovie.id));
        expect(store.getActions()).toEqual(expectedActions);
    });

    it('Should create REQUEST_MOVIE action and ERROR_RECEIVE_MOVIE action on successful fetch', async () => {

        fetchMock.get('*', Promise.reject('An error occurred.'));

        const expectedActions = [
            { type: currentMovieActions.REQUEST_MOVIE },
            { type: currentMovieActions.ERROR_RECEIVE_MOVIE, payload: 'Error' }
        ];

        const store = mockStore({ currentMovie: null });
        try {
            const result = await store.dispatch(currentMovieActions.fetchMovie(mockMovie.id));
        } catch (err) {
            expect(store.getActions()).toEqual(expectedActions);
        }
    });
});
