export const RESET_MOVIES = 'RESET_MOVIES';
export const REQUEST_ALL_MOVIES = 'REQUEST_ALL_MOVIES';
export const SUCCESS_RECEIVE_ALL_MOVIES = 'SUCCESS_RECEIVE_ALL_MOVIES';
export const ERROR_RECEIVE_ALL_MOVIES = 'ERROR_RECEIVE_ALL_MOVIES';
export const REQUEST_ALL_MOVIES_BY_GENRE = 'REQUEST_ALL_MOVIES_BY_GENRE';

export function requestAllMovies() {
    return {
        type: REQUEST_ALL_MOVIES
    };
}

export function successReceiveAllMovies(movies) {
    return {
        type: SUCCESS_RECEIVE_ALL_MOVIES,
        payload: movies
    };
}

export function errorReceiveAllMovies(error) {
    return {
        type: ERROR_RECEIVE_ALL_MOVIES,
        payload: error
    };
}

export function reset() {
    return {
        type: RESET_MOVIES
    };
}

export function fetchMovies(params) {

    return function (dispatch) {
        var url = new URL('http://react-cdp-api.herokuapp.com/movies');
        if (params) {
            if (params && params.search) {
                params.searchBy = params.searchBy || 'title';
            }

            Object.keys(params).forEach(key => {
                if (params[key]) {
                    url.searchParams.append(key, params[key]);
                }
            });
        }


        dispatch(requestAllMovies());

        return fetch(url)
            .then(response => response.json())
            .then((json =>
                dispatch(successReceiveAllMovies({ movies: json.data, total: json.total }))
            ))
            .catch(() => { dispatch(errorReceiveAllMovies('An error occurred.')); })
    };
}