import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as currentMoviesActions from '../../../actions/current-movie.actions';
import * as sortActions from '../../../actions/filter.actions';

import './movie.component.scss';

export class Movie extends React.Component {
    constructor() {
        super();
        this.onSearch = () => {
            this.props.history.push(`/`);
        }

        this.getYear = (releaseDate) => {
            return releaseDate && releaseDate.split('-')[0];
        }
        this.first = true;
    }

    componentDidMount() {
        if ((!this.props.movie) || (this.props.match.params.id !== this.props.movie.id)) {
            this.props.fetch(this.props.match.params.id)
        }
    }

    componentWillReceiveProps(newProps) {
        if ((!(newProps.movie && newProps.movie.id) || this.props.match.params.id === (newProps.movie && newProps.movie.id)) && !this.first) {
            newProps.history.push(`/notFound`);
        }
        if (newProps.match.params.id != (newProps.movie && newProps.movie.id)) {
            this.props.fetch(this.props.match.params.id);
            this.forceUpdate();
        }
        this.first = false;
    }

    render() {
        this.getYear();
        const content = !this.props.movie
            ? null
            : (
                <div className="movie">
                    <div className="movie__header">
                        <div className="movie__title">Netflixroulette</div>
                        <button onClick={this.onSearch} className="movie__button movies-db-button movies-db-button--accent">search</button>
                    </div>
                    <div className="movie__main">
                        <div className="movie__poster">
                            <img className="movie__img" src={this.props.movie.poster_path} />
                        </div>
                        <div className="movie__info">
                            <div className="movie__row">
                                <div className="movie__name">{this.props.movie.title}</div>
                                <div className="movie__rating">{this.props.movie.vote_average}</div>
                            </div>
                            <div className="movie__row">
                                <div className="movie__additiolal-info">
                                    {this.props.movie.tagline}
                                </div>
                            </div>
                            <div className="movie__row">
                                <div className="movie__year">
                                    {this.getYear(this.props.movie.release_date)}
                                </div>
                                <div className="movie__duration">
                                    {this.props.movie.runtime || '0'} min
                            </div>
                            </div>
                            <div className="movie__description movie__row">
                                {this.props.movie.overview}
                            </div>
                        </div>
                    </div>
                </div>);
        return (<Fragment>{content}</Fragment>);
    }
}

function mapStateToProps(state) {
    return {
        movie: state.currentMovie.currentMovie
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetch: (id) => {
            dispatch(currentMoviesActions.fetchMovie(id));
        }
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Movie))