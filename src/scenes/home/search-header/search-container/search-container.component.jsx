import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import * as sortActions from '../../../../actions/filter.actions';
import * as moviesActions from '../../../../actions/movies.actions';
import * as currentMoviesActions from '../../../../actions/current-movie.actions';

import './search-container.component.scss';

export class Search extends React.Component {
    constructor() {
        super();
        this.state = {
            value: ''
        };

        this.shouldFetch = false;

        this.handleChange = (event) => {
            this.setState({ value: event.target.value });
            this.props.search(this.state.value);
        }

        this.handleOnSearch = () => {
            this.search(this.props.sortState);
        }

        this.search = (sort) => {
            this.props.fetch(sort);
            const searchQuery = new URLSearchParams();
            searchQuery.append('search', sort.search);
            searchQuery.append('searchBy', sort.searchBy);

            this.props.history.push({
                pathname: '/search',
                search: searchQuery.toString()
            });
            this.forceUpdate();
        }
        this.setSearchByTitle = () => {
            this.props.searchByCategory('title');
            this.shouldFetch = true;
        }
        this.setSearchByGenre = () => {
            this.props.searchByCategory('genres');
            this.shouldFetch = true;
        }
        this.onKeyDown = (e) => {
            if (e.key === 'Enter') {
                this.search(this.props.sortState);
            }
        }
    }

    componentWillMount() {
        if (this.props.match && this.props.match.path.indexOf('/search') !== -1) {
            const query = new URLSearchParams(this.props.location.search);
            this.setState({ value: query.get('search') });
            this.props.search(query.get('search'));
            this.props.searchByCategory(query.get('searchBy'));
            this.shouldFetch = true;
        }
    }

    componentWillReceiveProps(newProps) {
        if (this.shouldFetch) {
            this.shouldFetch = false;
            this.search(newProps.sortState);
        }
    }

    render() {
        return (
            <div className="search">
                <div className="search__title">FIND YOUR MOVIE</div>
                <input className="search__field" value={this.state.value} onKeyDown={this.onKeyDown} onChange={this.handleChange} type="text" />
                <div className="search__controls">
                    <div className="search__filter search-filter">
                        <span className="search-filter__label">SEARCH BY</span>
                        <button
                            className={'search-filter__button movies-db-button ' + (this.state.searchBy === 'title' ? 'movies-db-button--accent' : '')}
                            onClick={this.setSearchByTitle}>TITLE</button>
                        <button
                            className={'search-filter__button movies-db-button ' + (this.state.searchBy !== 'title' ? 'movies-db-button--accent' : '')}
                            onClick={this.setSearchByGenre}>GENRE</button>
                    </div>
                    <button className="search__button movies-db-button movies-db-button--accent" onClick={this.handleOnSearch}>Search</button>
                </div>
            </div>);
    }
}

function mapStateToProps(state) {
    return {
        sortState: state.filter
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetCurrent: () => {
            dispatch(currentMoviesActions.closeMovie())
        },
        resetMovies: () => {
            dispatch(moviesActions.reset())
        },
        resetFilter: () => {
            return dispatch(sortActions.reset());
        },
        search: (value) => {
            return dispatch(sortActions.search(value));
        },
        fetch: (sort) => {
            return dispatch(moviesActions.fetchMovies(sort));
        },
        searchByCategory: (category) => {
            return dispatch(sortActions.searchByCategory(category));
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Search))