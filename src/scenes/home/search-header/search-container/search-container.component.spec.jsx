import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

import { initialState } from '../../../../reducers/reducer';
import thunk from 'redux-thunk';

import Search from './search-container.component';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Search', () => {
    const store = mockStore({ ...initialState });
    const search = shallow(<Provider store={store}><Search /></Provider>);

    it('should match snapshot', () => {
        expect(search).toMatchSnapshot();
    });

});