import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Home from './index';

describe('Home', () => {
    const home = shallow(<Home/>);

    it('should match snapshot', () => {
        expect(home).toMatchSnapshot();
    });
});