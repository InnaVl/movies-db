import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as sortActions from '../../actions/filter.actions';
import * as moviesActions from '../../actions/movies.actions';
import * as currentMoviesActions from '../../actions/current-movie.actions';
import Search from './search-header/search-container/search-container.component';

import './index.scss';


export class Home extends React.Component {
    componentDidMount() {
        if (this.props.match && this.props.match.path.indexOf('search') === -1) {
            this.props.resetMovies();
            this.props.resetCurrent();
            this.props.resetFilter();
        }
    }
    render() {
        return (
            <div className="header">
                <div className="header__title">Netflixroulette</div>
                <Search></Search>
            </div>);
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetCurrent: () => {
            dispatch(currentMoviesActions.closeMovie())
        },
        resetMovies: () => {
            dispatch(moviesActions.reset())
        },
        resetFilter: () => {
            return dispatch(sortActions.reset());
        },
        setSearch: (search) => {
            return dispatch(sortActions.search(search));
        },
        setSearchBy: (searchBy) => {
            return dispatch(sortActions.searchByCategory(searchBy));
        }
    }
}
export default withRouter(connect(null, mapDispatchToProps)(Home))