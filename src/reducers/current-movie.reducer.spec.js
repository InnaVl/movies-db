import { currentMovie, currentMovieInitialState } from './current-movie.reducer';
import * as currentMovieActions from '../actions/current-movie.actions'


describe('current movie reducer', () => {

    const mockMovie = {
        budget: 165000000,
        genres: ['Adventure', 'Drama', 'Science Fiction'],
        id: 157336,
        overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
        poster_path: 'https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
        release_date: '2014-11-05',
        revenue: 675120017,
        runtime: 169,
        tagline: 'Mankind was born on Earth. It was never meant to die here.',
        title: 'Interstellar',
        vote_average: 8.1,
        vote_count: 13627
    };

    it('should return the initial state', () => {
        expect(currentMovie(undefined, {})).toEqual(currentMovieInitialState)
    });

    it('should handle REQUEST_MOVIE', () => {
        const newState = {
            isLoading: true,
            error: '',
            currentMovie: null
        }
        expect(currentMovie(undefined, { type: currentMovieActions.REQUEST_MOVIE }))
            .toEqual(newState);
    });

    it('should handle SUCCESS_RECEIVE_MOVIE', () => {
        const newState = {
            isLoading: false,
            error: '',
            currentMovie: mockMovie
        }
        expect(currentMovie(undefined, {
            type: currentMovieActions.SUCCESS_RECEIVE_MOVIE,
            payload: mockMovie
        }))
            .toEqual(newState);
    });

    it('should handle ERROR_RECEIVE_MOVIE', () => {
        const newState = {
            isLoading: false,
            error: 'error',
            currentMovie: null
        }
        expect(currentMovie(undefined, {
            type: currentMovieActions.ERROR_RECEIVE_MOVIE,
            payload: 'error'
        }))
            .toEqual(newState);
    });
    it('should handle CLOSE_MOVIE', () => {
        const newState = {
            isLoading: false,
            error: '',
            currentMovie: null
        }
        expect(currentMovie(undefined, {
            type: currentMovieActions.CLOSE_MOVIE
        }))
            .toEqual(newState);
    });
});
