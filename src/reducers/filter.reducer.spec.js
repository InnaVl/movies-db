import { filter, filterInitialState } from './filter.reducer';
import * as filterActions from '../actions/filter.actions';


describe('filter reducer', () => {

    it('should return the initial state', () => {
        expect(filter(undefined, {})).toEqual(filterInitialState)
    });

    it('should handle SORT_BY_RATING', () => {
        const newState = {
            sortBy: 'rating',
            sortOrder: '',
            search: '',
            searchBy: '',
            filter: ''
        }
        expect(filter(undefined, {
            type: filterActions.SORT_BY_RATING
        }))
            .toEqual(newState);
    });

    it('should handle SORT_BY_RELEASE_DATE', () => {
        const newState = {
            sortBy: 'release_date',
            sortOrder: '',
            search: '',
            searchBy: '',
            filter: ''
        }
        expect(filter(undefined, {
            type: filterActions.SORT_BY_RELEASE_DATE
        }))
            .toEqual(newState);
    });

    it('should handle CHANGE_SORT_ORDER_DECS', () => {
        const newState = {
            sortBy: '',
            sortOrder: 'decs',
            search: '',
            searchBy: '',
            filter: ''
        }
        expect(filter(undefined, {
            type: filterActions.CHANGE_SORT_ORDER_DECS
        }))
            .toEqual(newState);
    });

    it('should handle CHANGE_SORT_ORDER_ASC', () => {
        const newState = {
            sortBy: '',
            sortOrder: 'asc',
            search: '',
            searchBy: '',
            filter: ''
        }
        expect(filter(undefined, {
            type: filterActions.CHANGE_SORT_ORDER_ASC
        }))
            .toEqual(newState);
    });

    it('should handle SEARCH', () => {
        const newState = {
            sortBy: '',
            sortOrder: '',
            search: 'Harry',
            searchBy: '',
            filter: ''
        }
        expect(filter(undefined, {
            type: filterActions.SEARCH,
            payload: 'Harry'
        }))
            .toEqual(newState);
    });

    it('should handle SEARCH_BY_CATEGORY', () => {
        const newState = {
            sortBy: '',
            sortOrder: '',
            search: '',
            searchBy: 'title',
            filter: ''
        }
        expect(filter(undefined, {
            type: filterActions.SEARCH_BY_CATEGORY,
            payload: 'title'
        }))
            .toEqual(newState);
    });

    it('should handle FILTER_BY_GENRE', () => {
        const newState = {
            sortBy: '',
            sortOrder: '',
            search: '',
            searchBy: '',
            filter: 'drama'
        }
        expect(filter(undefined, {
            type: filterActions.FILTER_BY_GENRE,
            payload: 'drama'
        }))
            .toEqual(newState);
    });
});