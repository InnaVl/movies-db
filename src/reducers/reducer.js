import { combineReducers } from 'redux';
import { movies, moviesInitialState } from './movies.reducer';
import { filter, filterInitialState } from './filter.reducer';
import { currentMovieInitialState, currentMovie } from './current-movie.reducer';

export const initialState = {
    movies: moviesInitialState,
    filter: filterInitialState,
    currentMovie: currentMovieInitialState
}
export const rootReducer = combineReducers({
    movies,
    filter,
    currentMovie
});

export default rootReducer;