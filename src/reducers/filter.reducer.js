import * as filterActions from '../actions/filter.actions';

export const filterInitialState = {
    sortBy: '',
    sortOrder: '',
    search: '',
    searchBy: '',
    filter: ''
}

export function filter(state = filterInitialState, action) {
    switch (action.type) {
        case filterActions.SORT_BY_RATING:
            return {
                ...state, sortBy: 'rating'
            };
        case filterActions.SORT_BY_RELEASE_DATE:
            return {
                ...state, sortBy: 'release_date'
            };
        case filterActions.CHANGE_SORT_ORDER_DECS:
            return {
                ...state, sortOrder: 'decs'
            };
        case filterActions.CHANGE_SORT_ORDER_ASC:
            return {
                ...state, sortOrder: 'asc'
            };
        case filterActions.SEARCH:
            return {
                ...state, search: action.payload
            };
        case filterActions.SEARCH_BY_CATEGORY:
            return {
                ...state, searchBy: action.payload
            };
        case filterActions.FILTER_BY_GENRE:
            return {
                ...state, filter: action.payload
            };
        case filterActions.RESET_FILTER:
            return filterInitialState;
        default: return state;
    }
}