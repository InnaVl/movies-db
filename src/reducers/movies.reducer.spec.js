import { movies, moviesInitialState } from './movies.reducer';
import * as moviesActions from '../actions/movies.actions';

const mockMovie = {
    budget: 165000000,
    genres: ['Adventure', 'Drama', 'Science Fiction'],
    id: 157336,
    overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
    poster_path: 'https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
    release_date: '2014-11-05',
    revenue: 675120017,
    runtime: 169,
    tagline: 'Mankind was born on Earth. It was never meant to die here.',
    title: 'Interstellar',
    vote_average: 8.1,
    vote_count: 13627
};

describe('Movies reducer', () => {

    it('should return the initial state', () => {
        expect(movies(undefined, {})).toEqual(moviesInitialState);
    });

    it('should handle REQUEST_ALL_MOVIES', () => {
        const newState = {
            movies: undefined,
            isLoading: true,
            error: '',
            total: 0
        }
        expect(movies(undefined, { type: moviesActions.REQUEST_ALL_MOVIES }))
            .toEqual(newState);
    });

    it('should handle SUCCESS_RECEIVE_ALL_MOVIES', () => {
        const newState = {
            movies: [mockMovie],
            isLoading: false,
            error: '',
            total: 1
        }
        expect(movies(undefined, {
            type: moviesActions.SUCCESS_RECEIVE_ALL_MOVIES,
            payload: {
                movies: [mockMovie],
                total: 1
            }
        }))
            .toEqual(newState);
    });

    it('should handle ERROR_RECEIVE_ALL_MOVIES', () => {
        const newState = {
            movies: undefined,
            isLoading: false,
            error: 'error',
            total: 0
        }
        expect(movies(undefined, {
            type: moviesActions.ERROR_RECEIVE_ALL_MOVIES,
            payload: 'error'
        }))
            .toEqual(newState);
    });

});
