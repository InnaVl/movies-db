import * as moviesActions from '../actions/movies.actions';

export const moviesInitialState = {
    movies: undefined,
    isLoading: false,
    error: '',
    total: 0
}

export function movies(state = moviesInitialState, action) {
    switch (action.type) {
        case moviesActions.REQUEST_ALL_MOVIES: {
            return {
                ...state, isLoading: true, error: '', total: 0
            }
        }
        case moviesActions.SUCCESS_RECEIVE_ALL_MOVIES:
            return {
                ...state, isLoading: false, movies: action.payload.movies, total: action.payload.total
            }
        case moviesActions.ERROR_RECEIVE_ALL_MOVIES:
            return {
                ...state, isLoading: false, error: action.payload
            }
        case moviesActions.RESET_MOVIES:
            return moviesInitialState;
            
        default: { return state; }
    }
}