import * as currentMoviesActions from '../actions/current-movie.actions';

export const currentMovieInitialState = {
    isLoading: false,
    error: '',
    currentMovie: null
}

export function currentMovie(state = currentMovieInitialState, action) {
    switch (action.type) {
        case currentMoviesActions.REQUEST_MOVIE:
            return {
                ...state, isLoading: true, error: ''
            }
        case currentMoviesActions.SUCCESS_RECEIVE_MOVIE:
            return {
                ...state, isLoading: false, error: '', currentMovie: action.payload
            }
        case currentMoviesActions.ERROR_RECEIVE_MOVIE:
            return {
                ...state, isLoading: false, error: action.payload
            }
        case currentMoviesActions.CLOSE_MOVIE:
            return {
                ...state, currentMovie: null
            }

        default: return state;
    }
}