import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router';

import { initialState } from './reducers/reducer';
import thunk from 'redux-thunk';

import { App } from './app.component';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('<App />', () => {
    describe('Should render App', () => {
        const mockMovie = {
            budget: 165000000,
            genres: ['Adventure', 'Drama', 'Science Fiction'],
            id: 157336,
            overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
            poster_path: 'https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
            release_date: '2014-11-05',
            revenue: 675120017,
            runtime: 169,
            tagline: 'Mankind was born on Earth. It was never meant to die here.',
            title: 'Interstellar',
            vote_average: 8.1,
            vote_count: 13627
        };

        const store = mockStore({ ...initialState, currentMovie: { currentMovie: mockMovie } });

        const app = render(<Provider store={store}>
            <MemoryRouter initialEntries={['/random']}><App />
            </MemoryRouter></Provider>);

        it('should match snapshot', () => {
            expect(app).toMatchSnapshot();
        });
    });
});