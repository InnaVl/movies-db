import React from 'react';
import { NavLink } from 'react-router-dom';
import './not-found.component.scss';

export class NotFound extends React.Component {
    render() {
        return (
            <div className="not-found">
                <div className="not-found__text">Page not found</div>
                <div className="not-found__link">
                    Return to <NavLink to="/">Home</NavLink>
                </div>
            </div >);
    }
}