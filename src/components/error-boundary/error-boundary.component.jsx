import React from 'react';
import './error-boundary.component.scss';

export default class ErrorBoundary extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasError: false
    };
  }

  componentDidCatch(error, info) {
    this.setState({
      hasError: true
    });
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className="error">
          <div className="error__img"></div>
          <div className="error__title">
            Something went wrong.
          </div>
          <div className="error__sub-title">
            You may refersh the page or try again later.
          </div>
        </div >
      );
    }
    return this.props.children;
  }
}
