import React from 'react';
import { shallow, mount, render } from 'enzyme';
import ErrorBoundary from './error-boundary.component';

describe('ErrorBoundary', () => {
    const errorBoundary = shallow(<ErrorBoundary />);

    it('should match snapshot (render error screen)', () => {
        errorBoundary.setState({ hasError: true });
        expect(errorBoundary).toMatchSnapshot();
    });

    it('should match snapshot (render child component)', () => {
        errorBoundary.setState({ hasError: false });
        expect(errorBoundary).toMatchSnapshot();
    });
});