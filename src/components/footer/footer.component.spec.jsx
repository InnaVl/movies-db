import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Footer from './footer.component';

describe('Footer', () => {
    const footer = shallow(<Footer />);

    it('should match snapshot', () => {
        expect(footer).toMatchSnapshot();
    });
});