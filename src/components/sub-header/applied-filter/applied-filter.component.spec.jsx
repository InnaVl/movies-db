import React from 'react';
import { shallow, mount, render } from 'enzyme';
import AppliedFilter from './applied-filter.component';

describe('AppliedFilter', () => {
    const mockProps = 'drama';
    const appliedFilter = shallow(<AppliedFilter genre={mockProps} />);

    it('should match snapshot', () => {
        expect(appliedFilter).toMatchSnapshot();
    });
});