import React from 'react';
import './applied-filter.component.scss';

export default class AppliedFilter extends React.Component {
    render() {
        return (<div className="applied-filter">Filterd by {this.props.genre} genre</div>);
    }
}