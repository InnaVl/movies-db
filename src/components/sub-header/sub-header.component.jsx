import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import Sorting from './sorting/sorting.component';
import AppliedFilter from './applied-filter/applied-filter.component';

import './sub-header.component.scss';

export class SubHeader extends React.Component {
    render() {
        const subHeader = !this.props.currentMovieState.currentMovie ? <Sorting /> : <AppliedFilter />
        return (
            <div className="sub-header">
                {subHeader}
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        currentMovieState: state.currentMovie,
    }
}

export default withRouter(connect(mapStateToProps)(SubHeader))