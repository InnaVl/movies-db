import React from 'react';
import { shallow, mount, render } from 'enzyme';
import SubHeader from './sub-header.component';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router';

import { initialState } from '../../reducers/reducer';
import thunk from 'redux-thunk';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('SubHeader', () => {
    it('should match snapshot', () => {
        const store = mockStore({ ...initialState });
        const subHeader = render(<Provider store={store}>
            <MemoryRouter initialEntries={['/random']}><SubHeader /></MemoryRouter>
        </Provider>);
        expect(subHeader).toMatchSnapshot();
    });
});