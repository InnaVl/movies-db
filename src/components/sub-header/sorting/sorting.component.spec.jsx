import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router';

import { initialState } from '../../../reducers/reducer';
import thunk from 'redux-thunk';

import Sorting from './sorting.component';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Sorting', () => {
    const store = mockStore({ ...initialState });
    const sorting = render(<Provider store={store}>
        <MemoryRouter initialEntries={['/random']}><Sorting /></MemoryRouter>
    </Provider>);

    it('should match snapshot', () => {
        expect(sorting).toMatchSnapshot();
    });

});