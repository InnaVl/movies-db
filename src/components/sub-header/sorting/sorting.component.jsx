import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import * as moviesActions from '../../../actions/movies.actions';
import * as sortingAction from '../../../actions/filter.actions';

import './sorting.component.scss';

export class Sorting extends React.Component {
    constructor() {
        super();
        this.onSortByReleaseDate = () => {
            this.props.sortByReleaseDate();
        }

        this.onSortByRating = () => {
            this.props.sortByRating();
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.sortState.sortBy !== this.props.sortState.sortBy) {
            this.props.fetch(this.props.sortState)
        }
    }
    render() {
        return (<div className="sorting">
            <div className="sorting__counter">{this.props.moviesState.total} movies found</div>
            <div className="sorting__by">
                Sort by:
                <span onClick={this.onSortByReleaseDate}>release date</span>
                <span onClick={this.onSortByRating}>rating</span>
            </div>
        </div>);
    }
}


function mapStateToProps(state) {
    return {
        sortState: state.filter,
        moviesState: state.movies,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {

        sortByReleaseDate: () => {
            return dispatch(sortingAction.sortByReleaseDate())
        },
        sortByRating: () => {
            return dispatch(sortingAction.sortByRating());
        },
        fetch: (sort) => {
            return dispatch(moviesActions.fetchMovies(sort))
        }

    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sorting))