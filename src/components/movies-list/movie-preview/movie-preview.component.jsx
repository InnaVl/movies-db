import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { NavLink } from 'react-router-dom'

import * as movieActions from '../../../actions/current-movie.actions';
import * as moviesActions from '../../../actions/movies.actions';
import * as sortActions from '../../../actions/filter.actions';

import './movie-preview.component.scss';

export class MoviePreview extends React.Component {
    componentWillReceiveProps(newProps) {
        if (newProps.sortState.filter !== this.props.sortState.filter) {
            this.props.fetch(newProps.sortState);
        }
    }

    render() {
        return (<NavLink className="movie-preview" to={`/movie/${this.props.movie.id}`}>
            <div className="movie-preview__poster">
                <img className="movie-preview__img"
                    alt={this.props.movie.title}
                    src={this.props.movie.poster_path} />
            </div>
            <div className="movie-preview__info">
                <div className="movie-preview__row">
                    <div className="movie-preview__name">{this.props.movie.title}</div>
                    <div className="movie-preview__year">{this.props.movie.release_date.split('-')[0]}</div>
                </div>
                <div className="movie-preview__row">
                    <div className="movie-preview__genre">{this.props.movie.genres.toString()}</div>
                </div>
            </div>
        </NavLink>);
    }
}

function mapStateToProps(state) {
    return {
        sortState: state.filter
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetch: (sort) => {
            dispatch(moviesActions.fetchMovies(sort))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MoviePreview))