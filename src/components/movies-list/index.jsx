import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'

import * as moviesActions from '../../actions/movies.actions';

import MoviesList from './list/list.component';
import EmptyPage from '../empty/empty.component';

export class Content extends React.Component {
    render() {
        const content = (this.props.moviesState.movies && this.props.moviesState.movies.length)
            ? <MoviesList movies={this.props.moviesState.movies} />
            : <EmptyPage />;

        return (
            <Fragment>
                {content}
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        moviesState: state.movies,
    }
}

export default withRouter(connect(mapStateToProps)(Content))