import React from 'react';
import { shallow, mount, render } from 'enzyme';
import MoviesList from './list.component';

describe('MoviesList', () => {
    const movie = {
        budget: 165000000,
        genres: ['Adventure', 'Drama', 'Science Fiction'],
        id: 157336,
        overview: 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.',
        poster_path: 'https://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg',
        release_date: '2014-11-05',
        revenue: 675120017,
        runtime: 169,
        tagline: 'Mankind was born on Earth. It was never meant to die here.',
        title: 'Interstellar',
        vote_average: 8.1,
        vote_count: 13627
    };
    const moviesList = shallow(<MoviesList movies={[movie]} />);

    it('should match snapshot', () => {
        expect(moviesList).toMatchSnapshot();
    });
});