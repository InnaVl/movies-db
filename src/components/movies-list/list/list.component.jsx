import React from 'react';
import './list.component.scss';

import MoviePreview from '../movie-preview/movie-preview.component';

export default class MoviesList extends React.Component {
    render() {
        return (<div className="movies-list">
            {
                this.props.movies.map(movie => <MoviePreview key={movie.id} movie={movie} onChangeState={this.onChangeState} />)
            }
        </div>);
    }
}
