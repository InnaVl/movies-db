import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Content from './index';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { MemoryRouter } from 'react-router';

import { initialState } from '../../reducers/reducer';
import thunk from 'redux-thunk';

import MoviesList from './list/list.component';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Content', () => {
    const store = mockStore({ ...initialState });
    it('should match snapshot', () => {
        const content = render(<Provider store={store}>
            <MemoryRouter initialEntries={['/random']}><Content /></MemoryRouter>
        </Provider>);
        expect(content).toMatchSnapshot();
    });

    // it('should match snapshot with Empty', () => {
    //     const content = shallow(<Content onChangeState={mockOnChangeState} isMovieOpen={false} />);
    //     expect(content).toMatchSnapshot();
    // });

    // it('should change state', () => {
    //     const mockMovie = {
    //         id: 1,
    //         name: 'movie1',
    //         year: '1999',
    //         genre: 'horror',
    //         poster: '',
    //         rating: 5,
    //         additionalInfo: 'additionalInfo',
    //         duration: '1h 30m',
    //         description: 'Lorem'
    //     };
    //     const content = shallow(<Content isMovieOpen={false} />);
    //     content.setProps({ onChangeState: () => { } });

    //     const mockState = {
    //         currentMovie: mockMovie
    //     };
    //     const mockOnChangeState = (mockMovie) => {
    //         content.setState(mockState);
    //         expect(content.state()).toEqual(mockState);
    //     };
    //     content.find(MoviesList).prop('onChangeState')(mockOnChangeState(mockMovie));
    // });
});