import React from 'react';
import './empty.component.scss';

export default function EmptyPage() {
    return React.createElement('div', { className: 'empty' }, 'No films found');
}