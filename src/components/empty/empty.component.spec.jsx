import React from 'react';
import { shallow, mount, render } from 'enzyme';
import EmptyPage from './empty.component';

describe('EMPTY', () => {
    const empty = shallow(<EmptyPage />);

    it('should match snapshot', () => {
        expect(empty).toMatchSnapshot();
    });

});